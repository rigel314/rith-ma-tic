package com.computingeureka.rith_ma_tic

import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    private var r = Random()
    private var res = 0
    private var nProbsComplete = 0
    private var elapsedTimeMs: Long = 0
    private var prefs: SharedPreferences? = null
    private var singleElapsed: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prefs = this.getSharedPreferences(PREFS_FILENAME, MODE_PRIVATE)
        switchAdd.isChecked = prefs?.getBoolean(ADD_BUTTON_STATE, true) != false
        switchSub.isChecked = prefs?.getBoolean(SUB_BUTTON_STATE, false) == true
        switchMul.isChecked = prefs?.getBoolean(MUL_BUTTON_STATE, false) == true
        switchDiv.isChecked = prefs?.getBoolean(DIV_BUTTON_STATE, false) == true

        res = prefs?.getInt(RES_STATE, 0) ?: 0
        problem.text = prefs?.getString(PROB_STATE, "0") ?: "0"

        nProbsComplete = prefs?.getInt(NPROBS_STATE, nProbsComplete) ?: nProbsComplete
        elapsedTimeMs = prefs?.getLong(ELAPSED_STATE, elapsedTimeMs) ?: elapsedTimeMs
        singleElapsed = 0 //prefs?.getLong(SINGLE_ELAPSED_STATE, singleElapsed) ?: singleElapsed

        updateAvg()

        entry.setOnEditorActionListener { v, actionId, event ->
            editorAction(v, actionId, event)
        }

        reset.setOnClickListener {
            nProbsComplete = 0
            elapsedTimeMs = 0
            singleElapsed = 0
            updateAvg()
            randomMathFill()
        }
    }

    private fun editorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
        if(actionId == EditorInfo.IME_ACTION_NEXT) {
            if(res == v.text.toString().toIntOrNull()) {
                var now = System.currentTimeMillis()
                if(singleElapsed != 0.toLong()) {
                    nProbsComplete++
                    elapsedTimeMs += (now - singleElapsed)
                }
                singleElapsed = now
                v.text = ""
                randomMathFill()
                updateAvg()
                return true
            }
        }
        return false
    }

    private fun randomMathFill() {
        var lbl = "0"
        var correct = 0
        var switches = booleanArrayOf(switchAdd.isChecked, switchSub.isChecked, switchMul.isChecked, switchDiv.isChecked)
        var nsw = -1

        switches.forEach {
            if(it) nsw++
        }

//        Log.i("nsw",nsw.toString())

        var n = if(nsw>0) r.nextInt(nsw+1) else 0
        var ch = 0
        switches.forEach {
            if(it) n--
            if(n >= 0) ch++
        }

//        Log.i("ch", ch.toString())

        when(ch) {
            0 -> { // swichAdd checked
                var a = r.nextInt(29)+1
                var b = r.nextInt(29)+1
                lbl = a.toString() + "+" + b.toString()
                correct = a + b
            }
            1 -> { // swichSub checked
                var a = r.nextInt(29)+1
                var b = r.nextInt(29)+1
                lbl = if(a > b) a.toString() + "-" + b.toString() else b.toString() + "-" + a.toString()
                correct = Math.abs(a - b)
            }
            2 -> { // swichMult checked
                var a = r.nextInt(10)
                var b = r.nextInt(11)
                lbl = a.toString() + "*" + b.toString()
                correct = a * b
            }
            3 -> { // swichDiv checked
                var a = r.nextInt(10)
                var b = r.nextInt(10)+1
                lbl = (a*b).toString() + "/" + b.toString()
                correct = a
            }
            else -> {
                // nothing for now
            }
        }

        res = correct
        problem.text = lbl
    }

    private fun updateAvg() {
        avgTime.text = "Avg: " + (if(nProbsComplete > 0) elapsedTimeMs/nProbsComplete else 0).toString() + " ms/ans"
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        prefs?.edit()?.run {
            putBoolean(ADD_BUTTON_STATE, switchAdd.isChecked)
            putBoolean(SUB_BUTTON_STATE, switchSub.isChecked)
            putBoolean(MUL_BUTTON_STATE, switchMul.isChecked)
            putBoolean(DIV_BUTTON_STATE, switchDiv.isChecked)

            putInt(RES_STATE, res)
            putString(PROB_STATE, problem.text.toString())

            putInt(NPROBS_STATE, nProbsComplete)
            putLong(ELAPSED_STATE, elapsedTimeMs)
            putLong(SINGLE_ELAPSED_STATE, singleElapsed)

            commit()
        }

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState)
    }

    companion object {
        const val ADD_BUTTON_STATE = "addButtonState"
        const val SUB_BUTTON_STATE = "subButtonState"
        const val MUL_BUTTON_STATE = "mulButtonState"
        const val DIV_BUTTON_STATE = "divButtonState"

        const val RES_STATE = "resState"
        const val PROB_STATE = "probState"

        const val NPROBS_STATE = "nProbsState"
        const val ELAPSED_STATE = "elapsedState"
        const val SINGLE_ELAPSED_STATE = "singleElapsedState"

        const val PREFS_FILENAME = "persist.prefs"
    }
}
